﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    // This allows the designer to modify the rate in which the asteriods will be spawned.
    public float rate;
    // Create's the array for the asteriod.
    public GameObject[] asteriod;
    // The number declared for the amount of waves.
    public int waves = 1;

    // Start is called before the first frame update
    void Start()
    {
        // Repeats the rate in which the asteriod is spawned.
        InvokeRepeating("spawnAsteriod", rate, rate);
    }

    // Update is called once per frame
    void spawnAsteriod()
    {
        // A for loop that spawns the asteriods within random areas within the x-axis.
        for (int i = 0; i < waves; i++)
            Instantiate(asteriod[(int)Random.Range(0, asteriod.Length)], new Vector3(Random.Range(-10.5f, 10.0f), 4, 0), Quaternion.identity);
    }
}
