﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteriodMovement : MonoBehaviour
{
    // Recognition that there is a Rigidbody2D with the game object.
    Rigidbody2D rb;
    // This allows the designer to modify the speed of the x-coordinate within the asteriod.
    public float xSpeed;
    // This allows the designer to modify the speed of the y-coordinate within the asteriod.
    public float ySpeed;
    // This allows the designer to modify the health of the sprite.
    public float health;

    private void Awake()
    {
        // Variable declaring that there is an attachment between the game object and the component of a Rigidbody2D.
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame.
    void Update()
    {
        // Let's the asteriod move depending on the speed.
        rb.velocity = new Vector2(xSpeed, ySpeed*-1);
    }

    // Recognition that there is collision between the asteriod and the ship, and that the ship will die after a certain amount of collisions.
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<shipMovement>().Damage();
            Die();
        }
    }

    // The line of code makes it so the asteriod is destroyed.
    void Die()
    {
        Destroy(gameObject);  
    }

    // This block of code makes it so health deceases everytime the ship collides with the asteriod.
    void Damage()
    {
        health--;
        if (health == 0)
        {
            Die();
        }
    }
}
